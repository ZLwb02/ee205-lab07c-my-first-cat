//###############################################################################
//###          University of Hawaii, College of Engineering
//### @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//###
//### @file    hello3.cpp
//### @version 1.0 - Initial implementation
//###
//### Build and test 3 Hello World Programs
//###
//### @author  @author Zack Lown <zlown@hawaii.edu>
//### @@date   2 March 2022
//###
//### @see     https://www.gnu.org/software/make/manual/make.html
//###############################################################################

using namespace std;
#include <iostream>

class Cat {
public:
   void sayHello() {
      cout << "Meow" << endl;
   } 
} ;



int main() {
   Cat myCat;
   myCat.sayHello();
   return 0;
}

